import React from "react";
import MainVideo from "../assets/main-video.mp4";
const VideoContainer = () => {
  return (
    <video
      style={{
        objectFit: "cover",
        display: "block",
        width: "100%",
        height: "100%",
      }}
      src={MainVideo}
      type="video/mp4"
      autoPlay
      muted
      loop
    />
  );
};

export default VideoContainer;
