import React from "react";
import "./About.css";
// Img
import AboutImg from "../../assets/img/about.jpg";
// Icon
import { AiTwotoneExperiment } from "react-icons/ai";
import { ImStackoverflow } from "react-icons/im";
import { BsAwardFill } from "react-icons/bs";

const About = () => {
  return (
    <div id="about" className="container about-container">
      {/* Title */}
      <div className="section-title">
        <h2 className="title">About Me</h2>
        <h2 className="sub-title">Get To Know</h2>
      </div>
      {/* Content */}
      <div className="content-wrapper">
        <div className="content-left">
          <div className="about-card">
            <div className="background"></div>
            <img src={AboutImg} alt="" className="about-img" />
          </div>
        </div>
        <div className="content-right">
          <div className="about-content-card-group">
            <div className="about-content-card">
              <div className="about-card-icon">
                <AiTwotoneExperiment />
              </div>
              <div className="about-card-title">Kinh Nghiệm</div>
              <div className="about-card-sub">Hơn 6 tháng</div>
            </div>
            <div className="about-content-card">
              <div className="about-card-icon">
                <ImStackoverflow />
              </div>
              <div className="about-card-title">Dự án cá nhân</div>
              <div className="about-card-sub">6+</div>
            </div>
            <div className="about-content-card">
              <div className="about-card-icon">
                <BsAwardFill />
              </div>
              <div className="about-card-title">Tốt nghiệp</div>
              <div className="about-card-sub">IronHack</div>
            </div>
          </div>
          <div className="about-content-description">
            Là một người yêu thích lập trình và đến nay đã hơn 6 tháng, xuất
            phát điểm là một người chuyển ngành , không biết gì về lập trình .
            Đến nay cũng đã tự mình code được nhiều dự án cá nhân từ nhỏ đến
            lớn. Dù chưa có kinh nghiệm thực tế nào , nhưng trong khoảng thời
            gian học ở trung tâm và tự học đã gặt hái được không ít kiến thức.
            Mong muốn được làm việc trong môi trường IT . Cố gắng hết sức để
            xứng đáng là lựa chọn của nhà tuyển dụng.
          </div>
          <div className="about-content-btn btn">
            <a href="#contact">Contact</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
