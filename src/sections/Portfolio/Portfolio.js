import React from "react";
import "./Portfolio.css";
// Img
import KaraShop from "../../assets/img/kara-shop.png";
import Pizza365 from "../../assets/img/pizza365.png";
import Mini from "../../assets/img/mini-project.png";
import Weather from "../../assets/img/weather-app.png";
import HiraStudio from "../../assets/img/hira-studio.png";
import MyPortfolio from "../../assets/img/portfolio.png";
const Portfolio = () => {
  return (
    <div id="portfolio" className="container portfolio-container">
      {/* Title */}
      <div className="section-title">
        <h2 className="title">My Project</h2>
        <h2 className="sub-title">Portfolio</h2>
      </div>
      {/* Content */}
      <div className="content-wrapper">
        <div className="portfolio-card-group">
          <div className="portfolio-card">
            <div className="portfolio-card-img-wrapper">
              <img src={KaraShop} alt="img project" />
            </div>
            <div className="portfolio-card-title">Fullstack Kara Shop 24h</div>
            <div className="btn-group ">
              <div className="btn">
                <a
                  href="https://gitlab.com/devcamp120.j2110/tailh"
                  target="_blank"
                >
                  Gitlab
                </a>
              </div>
            </div>
          </div>
          <div className="portfolio-card">
            <div className="portfolio-card-img-wrapper">
              <img src={Pizza365} alt="img project" />
            </div>
            <div className="portfolio-card-title">Pizza 365</div>
            <div className="btn-group ">
              <div className="btn">
                <a
                  href="https://gitlab.com/devcamp120.j2110/tailh/project-pizza-365-homepage"
                  target="_blank"
                >
                  Gitlab
                </a>
              </div>
            </div>
          </div>
          <div className="portfolio-card">
            <div className="portfolio-card-img-wrapper">
              <img src={Mini} alt="img project" />
            </div>
            <div className="portfolio-card-title">
              Mini Project ( 6 projects )
            </div>
            <div className="btn-group ">
              <div className="btn">
                <a
                  href="https://gitlab.com/devcamp120.j2110/tailh/mini-project"
                  target="_blank"
                >
                  Gitlab
                </a>
              </div>
            </div>
          </div>
          <div className="portfolio-card">
            <div className="portfolio-card-img-wrapper">
              <img
                style={{ height: "100%", objectFit: "cover" }}
                src={Weather}
                alt="img project"
              />
            </div>
            <div className="portfolio-card-title">Weather App</div>
            <div className="btn-group ">
              <div className="btn">
                <a
                  href="https://gitlab.com/devcamp120.j2110/tailh/weather-app"
                  target="_blank"
                >
                  Gitlab
                </a>
              </div>
            </div>
          </div>
          <div className="portfolio-card">
            <div className="portfolio-card-img-wrapper">
              <img src={HiraStudio} alt="img project" />
            </div>
            <div className="portfolio-card-title">Hira Studio</div>
            <div className="btn-group ">
              <div className="btn">
                <a
                  href="https://gitlab.com/devcamp120.j2110/tailh/hira-studio"
                  target="_blank"
                >
                  Gitlab
                </a>
              </div>
            </div>
          </div>
          <div className="portfolio-card">
            <div className="portfolio-card-img-wrapper">
              <img src={MyPortfolio} alt="img project" />
            </div>
            <div className="portfolio-card-title">My Portfolio</div>
            <div className="btn-group ">
              <div className="btn">
                <a
                  href="https://gitlab.com/devcamp120.j2110/tailh/my-portfolio"
                  target="_blank"
                >
                  Gitlab
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Portfolio;
