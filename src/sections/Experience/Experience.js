import React from "react";
import "./Experience.css";
// Icon
import { AiFillHtml5 } from "react-icons/ai";
import { DiCss3 } from "react-icons/di";
import { SiJavascript, SiMaterialui, SiMongodb } from "react-icons/si";
import { BsBootstrapFill } from "react-icons/bs";
import { FaReact, FaNodeJs } from "react-icons/fa";

const Experience = () => {
  return (
    <div id="experience" className="container experience-container">
      {/* Title */}
      <div className="section-title">
        <h2 className="title">My Experience</h2>
        <h2 className="sub-title">My Skills</h2>
      </div>
      <div className="content-wrapper">
        <div className="content-left">
          <div className="experience-card-group">
            <div className="content-title">Frontend Development</div>
            <div className="card">
              <div className="card-item">
                <i>
                  <AiFillHtml5 />
                </i>
                <div>
                  <h4>HTML</h4>
                  <h5>Experience</h5>
                </div>
              </div>
              <div className="card-item">
                <i>
                  <DiCss3 />
                </i>
                <div>
                  <h4>CSS</h4>
                  <h5>Experience</h5>
                </div>
              </div>
              <div className="card-item">
                <i>
                  <SiJavascript />
                </i>
                <div>
                  <h4>Javascript</h4>
                  <h5>Experience</h5>
                </div>
              </div>
              <div className="card-item">
                <i>
                  <BsBootstrapFill />
                </i>
                <div>
                  <h4>Bootstrap</h4>
                  <h5>Experience</h5>
                </div>
              </div>
              <div className="card-item">
                <i>
                  <FaReact />
                </i>
                <div>
                  <h4>React</h4>
                  <h5>Experience</h5>
                </div>
              </div>
              <div className="card-item">
                <i>
                  <SiMaterialui />
                </i>
                <div>
                  <h4>Material UI</h4>
                  <h5>Experience</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="content-right">
          <div className="experience-card-group">
            <div className="content-title">Backend Development</div>
            <div className="card">
              <div className="card-item">
                <i>
                  <FaNodeJs />
                </i>
                <div>
                  <h4>NodeJS</h4>
                  <h5>Experience</h5>
                </div>
              </div>
              <div className="card-item">
                <i>
                  <SiMongodb />
                </i>
                <div>
                  <h4>MongoDB</h4>
                  <h5>Experience</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Experience;
