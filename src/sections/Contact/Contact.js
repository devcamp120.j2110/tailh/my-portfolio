import React, { useRef } from "react";
import emailjs from "emailjs-com";
import "./Contact.css";
// Icon
import { BsFillTelephoneInboundFill, BsMessenger } from "react-icons/bs";
import { MdEmail } from "react-icons/md";

const Contact = () => {
  // Handle
  const form = useRef();
  const sendEmail = (e) => {
    e.preventDefault();
    emailjs
      .sendForm(
        "service_lmqz7u5",
        "template_ap1u21p",
        form.current,
        "sT7IJVtUs7w_XtJn8"
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
    e.target.reset();
  };

  return (
    <div id="contact" className="container contact-container">
      {/* Title */}
      <div className="section-title">
        <h2 className="title">Contact</h2>
        <h2 className="sub-title">Talk to me</h2>
      </div>
      {/* Content */}
      <div className="content-wrapper">
        <div className="content-left">
          {/* Info */}
          <h1>Thông Tin Liên Hệ</h1>
          <div>
            <BsFillTelephoneInboundFill /> Số điện thoại
            <h3>0904169341</h3>
          </div>
          <div>
            <MdEmail /> Email
            <h3>taile06121998@gmail.com</h3>
          </div>
          <div>
            <BsMessenger /> Messenger
            <h3>
              <a href="https://www.facebook.com/lehuutai0612" target="_blank">
                facebook.com/lehuutai0612
              </a>
            </h3>
          </div>
        </div>
        <div className="content-right">
          <form ref={form} onSubmit={sendEmail}>
            <input
              type="text"
              name="name"
              placeholder="Họ tên"
              required
            />
            <input
              type="email"
              name="email"
              placeholder="Email"
              required
            />
            <textarea
              name="message"
              rows="7"
              placeholder="Message"
              required
            ></textarea>
            <button type="submit" className="btn">
              Send Message
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Contact;
