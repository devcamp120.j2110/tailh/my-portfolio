import React from "react";
import VideoContainer from "../../components/VideoContainer";
import "./Header.css";
// Framer Motion
import { motion } from "framer-motion";
// Cover letter
import CV from "../../assets/LE-HUU-TAI-CV.pdf";

// Variants
const titleVariants = {
  hidden: {
    opacity: 0,
  },
  show: {
    opacity: 1,
    transition: {
      delayChildren: 1,
      staggerChildren: 0.3,
    },
  },
};
const item = {
  hidden: {
    opacity: 0,
  },
  show: {
    opacity: 1,
  },
};

const Header = () => {
  return (
    <div className="container header-container">
      <div className="overlay"></div>
      <VideoContainer />
      <motion.div
        className="header-content"
        variants={titleVariants}
        initial="hidden"
        animate="show"
      >
        <div>
          <motion.h1 variants={item}>H</motion.h1>
          <motion.h1 variants={item}>u</motion.h1>
          <motion.h1 variants={item}>u</motion.h1>
          <motion.h1 variants={item}>T</motion.h1>
          <motion.h1 variants={item}>a</motion.h1>
          <motion.h1 variants={item}>i</motion.h1>
        </div>
        <motion.h2 variants={item}>Web Developer</motion.h2>
        <motion.h3 variants={item}>Create, Develop, Inspire</motion.h3>
        <motion.div variants={item} className="btn-group header-btn">
          <div className="btn">
            <a href={CV} download>
              Download CV
            </a>
          </div>
          <div className="btn">
            <a href="#contact">Contact</a>
          </div>
        </motion.div>
      </motion.div>
    </div>
  );
};

export default Header;
