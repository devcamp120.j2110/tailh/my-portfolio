import "./App.css";
// Framer Motion
import { AnimatePresence } from "framer-motion";
// Component
import Header from "./sections/Header/Header";
import About from "./sections/About/About";
import Experience from "./sections/Experience/Experience";
import Portfolio from "./sections/Portfolio/Portfolio";
import Contact from "./sections/Contact/Contact";
import Navbar from "./sections/Navbar/Navbar";

function App() {
  return (
    <AnimatePresence>
      <main>
        <Header />
        <About />
        <Experience />
        <Portfolio />
        <Contact />
        <Navbar />
      </main>
    </AnimatePresence>
  );
}

export default App;
